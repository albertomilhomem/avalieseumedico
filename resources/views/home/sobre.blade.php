@extends('layouts.default')
@section('head')
<title>Avalie Seu Médico - Sobre</title>
@endsection

@section('content')
<div class="container">
	<div class="col-sm-offset-1 col-sm-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				Sobre
			</div>

			<div class="panel-body">		

				<p>Avalie Seu Médico é uma aplicação desenvolvida com o objetivo de auxiliar as pessoas na busca por médicos de confianças.</p>
			</div>
		</div>
	</div>
</div>

@endsection