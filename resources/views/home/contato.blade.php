@extends('layouts.default')
@section('head')
<title>Avalie Seu Médico - Sobre</title>
@endsection

@section('content')
<div class="container">
	<div class="col-sm-offset-1 col-sm-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				Contato
			</div>

			<div class="panel-body">	
				<p>Facebook: AvalieSeuMedico</p>
				<p>Twitter: AvalieSeuMedico</p>
				<p>E-mail: contato@avalieseumedico.com.br</p>
			</div>
		</div>
	</div>
</div>

@endsection