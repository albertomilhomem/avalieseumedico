    
@extends('layouts.default')
@section('head')
<title>Avalie Seu Médico - Página não encontrada</title>
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<style>
    .title {
        font-size: 40px;
        margin-bottom: 40px;            
        font-weight: 100;
        font-family: 'Lato';
    }
</style>
</head>
@endsection

@section('content')
<div class="container">
    <div class="container">
        <div class="content">
            <div class="title text-center">
                <p>Erro 404 - Está página não pode ser encontrada!</p>
            </div>
        </div>
    </div>
</div>
@endsection
