<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EspecialidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|max:100',
            'descricao' => 'required|max:300'
        ];
    }

    public function messages()
    {
        return [
        'required' => 'O atributo :attribute não pode estar em branco.'
        ];
    }
}
